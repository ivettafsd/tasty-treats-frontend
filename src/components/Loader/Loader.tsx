import React from "react";
import CircularProgress from "@mui/material/CircularProgress";

const Loader: React.FC = () => {
    return (
      <div>
        <CircularProgress style={{ color: "#9BB537" }} size={40} />
      </div>
    );
};

export default Loader;
