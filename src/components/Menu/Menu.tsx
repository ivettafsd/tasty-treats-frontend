import React from "react";
import Navigation from '../Navigation';
import { MenuProps } from '../../types/data';
import styles from './Menu.module.scss';

const Menu: React.FC<MenuProps> = ({ onToggleMenu }) => {
  return (
    <div className={styles.menu}>
      <Navigation onToggleMenu={onToggleMenu} />
    </div>
  );
};

export default Menu;
