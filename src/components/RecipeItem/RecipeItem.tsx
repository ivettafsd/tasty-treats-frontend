import React, { useState, useEffect } from "react";
import { uid } from 'uid';
import Modal from "../Modal";
import RecipeModal from "../RecipeModal";
import { getRecipeById } from "../../services";
import { RecipeItemProps, Recipe } from "../../types/data";
import sprite from "../../assets/images/sprite.svg";
import styles from "./RecipeItem.module.scss";
import io from "socket.io-client";

const RecipeItem: React.FC<RecipeItemProps> = ({ recipe }) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [fullDataRecipe, setFullDataRecipe] = useState(null);
  const [currentFavorites, setCurrentFavorites] = useState<Recipe[]>([]);

  useEffect(() => {
    const isAddedToLocalStorage: string | null = localStorage.getItem("favorites");
    if (isAddedToLocalStorage) {
      setCurrentFavorites(JSON.parse(isAddedToLocalStorage));
    }
  }, [localStorage]);

  const handleToggleFavorites = (): void => {
    const isAddedToLocalStorage: string | null = localStorage.getItem("favorites");
    const actualCurrentRecipes = isAddedToLocalStorage ? JSON.parse(isAddedToLocalStorage) : [];
    let updatedFavorites: Recipe[];

    if (actualCurrentRecipes.find((currentRecipe: Recipe) => currentRecipe._id === recipe._id)) {
      updatedFavorites = actualCurrentRecipes.filter((favoriteRecipe: Recipe) => favoriteRecipe._id !== recipe._id);
      setCurrentFavorites(updatedFavorites);
    } else {
      updatedFavorites = [...actualCurrentRecipes, recipe];
      setCurrentFavorites((prev: Recipe[]) => {
        return [...prev, recipe];
      });
    }

    localStorage.setItem("favorites", JSON.stringify(updatedFavorites));
  };
  const handleOpenRecipeModal = async () => {
    const openRecipe = await getRecipeById(recipe._id);
    setFullDataRecipe(openRecipe);
    setIsModalOpen(true);
  };

  const handleCloseRecipeModal = () => {
    setIsModalOpen(false);
  };

  const socket = io("https://tasty-treats-backend.onrender.com");

  useEffect(() => {
    socket.emit("testSockets");
  }, []);


  return (
    <>
      <li className={styles.card}>
        <img className={styles.img} src={recipe.thumb} width={335} height={335} />
        <div className={styles.box}>
          <button className={`reset-btn ${styles.heart}`} type="button" onClick={handleToggleFavorites}>
            <svg
              className={
                currentFavorites.find((currentRecipe: Recipe) => currentRecipe._id === recipe._id)
                  ? styles.activeHeart
                  : styles.heart
              }
              width={22}
              height={22}
            >
              <use href={`${sprite}#icon-heart`}></use>
            </svg>
          </button>
          <div className={styles.content}>
            <h3 className={styles.title}>{recipe.title}</h3>
            <p className={styles.description}>{recipe.description.slice(0, 90) + "..."}</p>
            <div className={styles.toolbar}>
              <div className={styles.rating}>
                {[1, 2, 3, 4, 5].map((rate: number) => (
                  <svg
                    className={Math.round(recipe.rating) >= rate ? styles.activeStar : styles.star}
                    width={18}
                    height={18}
                    key={uid()}
                  >
                    <use href={`${sprite}#icon-star`}></use>
                  </svg>
                ))}
              </div>

              <button className={styles.btn} type="button" onClick={handleOpenRecipeModal}>
                See recipe
              </button>
            </div>
          </div>
        </div>
      </li>
      {isModalOpen && fullDataRecipe && (
        <Modal onClose={handleCloseRecipeModal}>
          <RecipeModal recipe={fullDataRecipe} />
        </Modal>
      )}
    </>
  );
};

export default RecipeItem;
