import React from "react";
import { Link } from "react-router-dom";
import { NavigationProps } from '../../types/data';
import styles from "./Navigation.module.scss";

const Navigation: React.FC<NavigationProps> = ({ onToggleMenu }) => {
  return (
    <nav className={styles.nav}>
      <Link onClick={onToggleMenu} className={styles.link} to="/">
        Home
      </Link>
      <Link onClick={onToggleMenu} className={styles.link} to="/favorites">
        Favorites
      </Link>
    </nav>
  );
};

export default Navigation;
