import React from "react";
import RecipeItem from "../RecipeItem";
import { RecipesListProps, Recipe } from "../../types/data";
import styles from './RecipeList.module.scss';

const RecipesList: React.FC<RecipesListProps> = ({ recipes = [] }) => {
  return (
    <ul className={styles.list}>
      {recipes.map((recipe: Recipe) => (
        <RecipeItem key={recipe._id} recipe={recipe} />
      ))}
    </ul>
  );
};

export default RecipesList;
