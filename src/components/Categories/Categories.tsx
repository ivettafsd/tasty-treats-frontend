import React, { useState } from "react";
import { useAsync } from "react-use";
import Loader from "../Loader";
import { getCategories } from "../../services";
import { Category } from "../../types/data";
import styles from "./Categories.module.scss";

interface CategoriesProps {
  onClick: (category: string | null) => void;
  currentCategory: string | null;
}

const Categories: React.FC<CategoriesProps> = ({ onClick, currentCategory }) => {
  const [categories, setCategories] = useState<Category[]>([]);

  useAsync(async () => {
    const dataCategories = await getCategories();
    setCategories(dataCategories);
  }, []);

  return (
    <ul className={styles.categories}>
      <li>
        <button
          className={currentCategory ? styles.all : `${styles.all} ${styles.active}`}
          type="button"
          onClick={() => onClick(null)}
        >
          All categories
        </button>
      </li>
      <div className={styles.categoriesList}>
        {categories.length > 0 ? (
          categories.map((category: Category) => (
            <li key={category._id}>
              <button className={styles.category} type="button" onClick={() => onClick(category.name)}>
                {category.name}
              </button>
            </li>
          ))
        ) : (
          <Loader />
        )}
      </div>
    </ul>
  );
};

export default Categories;
