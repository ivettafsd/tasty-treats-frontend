import React, { useState, useEffect } from "react";
import { useAsync } from "react-use";
import {uid} from 'uid';
import Modal from "../Modal";
import RatingModal from "../RatingModal";
import { RecipeModalProps } from "../../types/data";
import { getRecipeById } from "../../services";
import { FullRecipe, IngredientForDish, Recipe } from "../../types/data";
import sprite from "../../assets/images/sprite.svg";
import styles from "./RecipeModal.module.scss";

const RecipeModal: React.FC<RecipeModalProps> = ({ recipe }) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [fullRecipe, setFullRecipe] = useState<FullRecipe | null>(null);
  const [currentFavorites, setCurrentFavorites] = useState<Recipe[]>([]);

  useEffect(() => {
    const isAddedToLocalStorage: string | null = localStorage.getItem("favorites");
    if (isAddedToLocalStorage) {
      setCurrentFavorites(JSON.parse(isAddedToLocalStorage));
    }
  }, [localStorage]);

  useAsync(async () => {
    if (recipe) {
      const dataFullRecipe = await getRecipeById(recipe._id);
      setFullRecipe(dataFullRecipe);
    }
  }, [recipe]);

  const handleToggleFavorites = (): void => {
    const isAddedToLocalStorage: string | null = localStorage.getItem("favorites");
    const actualCurrentRecipes = isAddedToLocalStorage ? JSON.parse(isAddedToLocalStorage) : [];
    let updatedFavorites: Recipe[];

    if (recipe) {
      if (actualCurrentRecipes.find((currentRecipe: Recipe) => currentRecipe._id === recipe._id)) {
        updatedFavorites = actualCurrentRecipes.filter((favoriteRecipe: Recipe) => favoriteRecipe._id !== recipe._id);
      } else {
        updatedFavorites = [...actualCurrentRecipes, recipe];
      }
      setCurrentFavorites(updatedFavorites);
      localStorage.setItem("favorites", JSON.stringify(updatedFavorites));
    }
  };
  const handleOpenRatingModal = async () => {
    setIsModalOpen(true);
  };
  const handleCloseRatingModal = () => {
    setIsModalOpen(false);
  };
  return (
    <>
      {fullRecipe && (
        <div>
          <video>
            <source src={fullRecipe.youtube} type="video/webm" />
          </video>
          <h3 className={styles.title}>{fullRecipe.title}</h3>
          <div className={styles.ratingBox}>
            <span className={styles.value}>{Math.round(fullRecipe.rating)}</span>
            {[1, 2, 3, 4, 5].map((rate: number) => (
              <svg
                className={Math.round(fullRecipe.rating) >= rate ? styles.activeStar : styles.star}
                width={18}
                height={18}
                key={uid()}
              >
                <use href={`${sprite}#icon-star`}></use>
              </svg>
            ))}

            <p className={styles.time}>{fullRecipe.time}min</p>
          </div>
          <ul className={styles.ingredients}>
            {fullRecipe.ingredients.map((ingredient: IngredientForDish) => (
              <li key={ingredient.id}>
                {ingredient.name}
                <span className={styles.measure}>{ingredient.measure}</span>
              </li>
            ))}
          </ul>
          {fullRecipe.tags.length > 0 && (
            <ul className={styles.tags}>
              {fullRecipe.tags.map((tag) => (
                <li key={tag} className={styles.tag}>
                  #{tag}
                </li>
              ))}
            </ul>
          )}
          <p className={styles.instructions}>{fullRecipe.instructions}</p>
          <div>
            <button className={`reset-btn ${styles.favorite}`} type="button" onClick={handleToggleFavorites}>
              {currentFavorites?.find((currentRecipe: Recipe) => currentRecipe._id === fullRecipe._id)
                ? "Remove from favorite"
                : "Add to favorite"}
            </button>
            <button className={`reset-btn ${styles.give}`} type="button" onClick={handleOpenRatingModal}>
              Give a rating
            </button>
          </div>
        </div>
      )}
      {isModalOpen && fullRecipe && (
        <Modal onClose={handleCloseRatingModal}>
          <RatingModal />
        </Modal>
      )}
    </>
  );
};

export default RecipeModal;
