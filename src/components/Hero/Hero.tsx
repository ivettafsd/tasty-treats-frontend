import React, { useState } from "react";
import { useAsync } from "react-use";
import { uid } from 'uid';
import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination } from "swiper";
import Loader from '../Loader';
import { getEvents } from "../../services";
import { Event, HeroProps } from "../../types/data";
import styles from "./Hero.module.scss";

const Hero: React.FC<HeroProps> = ({ onToggleModal }) => {
  const [events, setEvents] = useState([]);

  useAsync(async () => {
    const dataEvents = await getEvents();
    setEvents(dataEvents);
  }, []);

   const pagination = {
     clickable: true,
     renderBullet: function (index: number, className: string) {
  
       return `<span class="${className} ${styles.bullet}" key="${uid()}"></span>`;
     },
   };
  
  return (
    <section className={styles.hero}>
      <div className={styles.content}>
        <h1 className={styles.title}>
          Learn to Cook <span className={styles.accent}>Tasty Treats'</span> Customizable Masterclass
        </h1>
        <h2 className={styles.subtitle}>
          TastyTreats - Customize Your Meal with Ingredient Options and Step-by-Step Video Guides.
        </h2>
        <button className={`reset-btn ${styles.order}`} onClick={onToggleModal}>
          Order now
        </button>
      </div>
      <Swiper
        spaceBetween={8}
        slidesPerView={0.8}
        pagination={pagination}
        modules={[Pagination]}
        scrollbar={{ draggable: true }}
        className={styles.slider}
      >
        {events.length > 0 ? (
          events.map((event: Event) => (
            <React.Fragment key={event.cook.imgUrl}>
              <SwiperSlide className={styles.slide} key={event.topic.previewWebpUrl}>
                <div className={styles.slideCook}>
                  <img src={event.cook.imgWebpUrl} alt={event.cook.name} height={290} />
                </div>
                <div className={styles.slideTopicPreview}>
                  <img src={event.topic.previewWebpUrl} alt={event.topic.name} width={150} />
                </div>
                <div className={styles.slideTopicImg}>
                  <img src={event.topic.imgWebpUrl} alt={event.topic.name} width={500} />
                </div>
              </SwiperSlide>
            </React.Fragment>
          ))
        ) : (
          <Loader />
        )}
      </Swiper>
    </section>
  );
};

export default Hero;
