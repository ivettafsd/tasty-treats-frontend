import React, { useState, useEffect } from "react";
import { useAsync } from "react-use";
import { useDebounce } from "../../hooks/useDebounce";
import Loader from "../Loader";
import { getAreas, getIngredients } from "../../services";
import { Ingredient, Area, ToolbarProps } from "../../types/data";
import sprite from "../../assets/images/sprite.svg";
import styles from "./Toolbar.module.scss";

const Toolbar: React.FC<ToolbarProps> = ({
  setCurrentSearch,
  currentArea,
  setCurrentArea,
  currentIngredient,
  setCurrentIngredient,
  currentTime,
  setCurrentTime,
}) => {
  const [search, setSearch] = useState("");
  const [areas, setAreas] = useState([]);
  const [ingredients, setIngredients] = useState([]);

  useAsync(async () => {
    const dataAreas = await getAreas();
    setAreas(dataAreas);
    const dataIngredients = await getIngredients();
    setIngredients(dataIngredients);
  }, []);

  const debounceSearch = useDebounce(search, 500);

  useEffect(() => {
    setCurrentSearch(debounceSearch);
  }, [debounceSearch]);

  const handleResetFilters = () => {
    setSearch("");
    setCurrentSearch("");
    setCurrentArea("");
    setCurrentIngredient("");
    setCurrentTime("");
  };

  return (
    <section className={styles.filters}>
      {areas.length > 0 && ingredients.length > 0 ? (
        <>
          <div className={styles.field}>
            <label className={styles.label} htmlFor="search">
              Search:
            </label>
            <input
              id="search"
              placeholder="Enter text"
              type="text"
              value={search}
              onChange={({ target }) => setSearch(target.value)}
            />
          </div>

          <div className={styles.subField}>
            <div className={styles.field}>
              <label className={styles.label} htmlFor="time">
                Time:
              </label>
              <select id="time" value={currentTime || ""} onChange={({ target }) => setCurrentTime(target.value)}>
                <option value="">Choose time</option>
                {Array.from({ length: 120 / 5 }, (_, index) => (index + 1) * 5).map((time) => (
                  <option key={time} value={time}>
                    {time}min
                  </option>
                ))}
              </select>
            </div>
            <div className={styles.field}>
              <label className={styles.label} htmlFor="area">
                Area:
              </label>
              {areas.length > 0 && (
                <select id="area" value={currentArea || ""} onChange={({ target }) => setCurrentArea(target.value)}>
                  <option value="">Choose area</option>
                  {areas.map((area: Area) => (
                    <option key={area._id} value={area.name}>
                      {area.name}
                    </option>
                  ))}
                </select>
              )}
            </div>
          </div>
          <div className={`${styles.field} ${styles.ingredients}`}>
            <label className={styles.label} htmlFor="ingredients">
              Ingredients:
            </label>
            {ingredients.length > 0 && (
              <select
                id="ingredients"
                value={currentIngredient || ""}
                onChange={({ target }) => setCurrentIngredient(target.value)}
              >
                <option value="">Choose ingredient</option>
                {ingredients.map((ingredient: Ingredient) => (
                  <option key={ingredient._id} value={ingredient._id}>
                    {ingredient.name}
                  </option>
                ))}
              </select>
            )}
          </div>
        </>
      ) : (
        <Loader />
      )}
     {  (search || currentArea || currentIngredient || currentTime) &&  <button className={styles.reset} type="submit" onClick={handleResetFilters}>
        <svg width="16" height="16">
          <use href={`${sprite}#icon-cross`}></use>
        </svg>
        Reset the filter
      </button>}
    </section>
  );
};

export default Toolbar;
