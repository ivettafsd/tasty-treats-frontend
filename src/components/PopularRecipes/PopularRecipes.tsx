import React, { useState } from "react";
import { useAsync } from "react-use";
import Modal from "../Modal";
import RecipeModal from "../RecipeModal";
import Loader from "../Loader";
import { getPopularRecipes, getRecipeById } from "../../services";
import { PopularRecipe } from "../../types/data";
import styles from "./PopularRecipes.module.scss";

const PopularRecipes: React.FC = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [fullDataRecipe, setFullDataRecipe] = useState(null);
  const [popularRecipes, setPopularRecipes] = useState([]);

  useAsync(async () => {
    const recipes = await getPopularRecipes();
    setPopularRecipes(recipes);
  }, []);

  const handleOpenRecipeModal = async (recipeId: string) => {
    const openRecipe = await getRecipeById(recipeId);
    setFullDataRecipe(openRecipe);
    setIsModalOpen(true);
  };

  const handleCloseRecipeModal = () => {
    setIsModalOpen(false);
  };

  return (
    <section className={styles.popular}>
      <h3 className={styles.title}>Popular recipes</h3>
      <ul className={styles.list}>
        {popularRecipes.length > 0 ?
          popularRecipes.map((recipe: PopularRecipe) => (
            <li className={styles.card} key={recipe._id} onClick={() => handleOpenRecipeModal(recipe._id)}>
              <img className={styles.img} src={recipe.thumb} alt={recipe.title} />
              <div>
                <h3 className={styles.name}>
                  {recipe.title.length > 26 ? recipe.title.slice(0, 26) + "..." : recipe.title}
                </h3>
                <p className={styles.description}>
                  {recipe.description.length > 65 ? recipe.description.slice(0, 65) + "..." : recipe.description}
                </p>
              </div>
            </li>
          )) :<Loader/>}
      </ul>
      {isModalOpen && fullDataRecipe && (
        <Modal onClose={handleCloseRecipeModal}>
          <RecipeModal recipe={fullDataRecipe} />
        </Modal>
      )}
    </section>
  );
};
export default PopularRecipes;
