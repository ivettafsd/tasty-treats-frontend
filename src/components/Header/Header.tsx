import React from "react";
import Logo from "../Logo";
import { HeaderProps } from '../../types/data';
import sprite from '../../assets/images/sprite.svg';
import styles from './Header.module.scss';

const Header: React.FC<HeaderProps> = ({ onToggleModal, onToggleMenu }) => {
  return (
    <header className={styles.header}>
      <Logo />
      <div className={styles.btns}>
        <button className={`reset-btn ${styles.cart}`} onClick={onToggleModal}>
          <svg width="24" height="24">
            <use href={`${sprite}#icon-cart`}></use>
          </svg>
        </button>
        <button className={`reset-btn ${styles.menu}`} onClick={onToggleMenu}>
          <svg width="32" height="32">
            <use href={`${sprite}#icon-menu`}></use>
          </svg>
        </button>
      </div>
    </header>
  );
};

export default Header;
