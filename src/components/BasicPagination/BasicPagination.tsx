import React from "react";
import Pagination from "@mui/material/Pagination";
import { BasicPaginationProps } from "../../types/data";
import styles from "./BasicPagination.module.scss";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiPagination-ul": {
      justifyContent: "center",
      //marginTop: theme.spacing(2),
    },
    "& .MuiPaginationItem-root": {
      color: "#050505",
      fontWeight: "600",
      border: "1px solid rgba(5, 5, 5, 0.5)",
      padding: "9px",
      width: "40px",
      height: "40px",
      borderRadius: "100%",
    },
    "& .MuiPaginationItem-root.Mui-selected": {
      //backgroundColor: theme.palette.primary.main,
      border: "1px solid transparent",
      backgroundColor: "#9BB537",
      color: "#050505",
      "&:hover": {
        border: "1px solid transparent",
        backgroundColor: "rgba(0, 0, 0, 0.04)",
        //backgroundColor: theme.palette.primary.dark,
      },
    },
    "& .MuiPaginationItem-previousNext": {
      border: "1px solid transparent",
      backgroundColor: "#9BB537",
      opacity: 1,
    },
    "& .MuiPaginationItem-firstLast": {
      border: "1px solid transparent",
      backgroundColor: "#9BB537",
      opacity: 1,
    },
    "& .MuiPaginationItem-root.Mui-disabled": {
      color: "rgba(5, 5, 5, 0.5)",
      backgroundColor: "#000",
      opacity: 1,
    },
    "& .MuiPaginationItem-root.Mui-disabled .MuiPaginationItem-icon": {
      color: "rgba(255, 255, 255, 0.5);",
      opacity: 1,
    },
  },
}));

const BasicPagination: React.FC<BasicPaginationProps> = ({ totalPages, setCurrentPage }) => {
  const classes = useStyles();

  const handleChange = (value: number) => {
    setCurrentPage(value);
  };

  return (
    <div className={styles.pagination}>
      <Pagination
        count={totalPages}
        color="primary"
        onChange={(e, value) => handleChange(value)}
        classes={{ root: classes.root }}
        showFirstButton
        showLastButton
        siblingCount={0}
        boundaryCount={0}
      />
    </div>
  );
};

export default BasicPagination;
