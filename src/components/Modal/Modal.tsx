import React, { useEffect, useRef, MouseEvent, KeyboardEvent } from "react";
import ReactDOM from "react-dom";
import { ModalProps } from "../../types/data";
import sprite from "../../assets/images/sprite.svg";
import styles from "./Modal.module.scss";

const Modal: React.FC<ModalProps> = ({ onClose, children, fullVP = false }) => {
  const targetElement = document.getElementById("modal-root") as Element;
  const backdrop = useRef<HTMLDivElement>(null);

  const handleClickOutside = (event: MouseEvent) => {
    if (event.target === backdrop.current) {
      onClose();
    }
    document.body.style.overflow = "";
    document.body.style.position = "";
    event.stopPropagation();
  };

  const handleKeyDown = (event: KeyboardEvent<HTMLDivElement> | any) => {
    if (event.key === "Escape") {
      document.body.style.overflow = "";
      document.body.style.position = "";
      onClose();
    }
  };

  useEffect(() => {
    const eventHandler = (e: KeyboardEvent<HTMLDivElement> | any) => handleKeyDown(e);
    document.addEventListener("keydown", eventHandler);
    document.body.style.overflow = "hidden";
    return () => {
      document.removeEventListener("keydown", eventHandler);
    };
  }, []);

  return ReactDOM.createPortal(
    <div className={fullVP ? styles.menu : styles.modal} onClick={handleClickOutside} ref={backdrop}>
      <div className={styles.cover}>
        <div className={styles.content}>
          <button className={styles.close} onClick={onClose}>
            <svg width="20" height="20">
              <use href={`${sprite}#icon-cross`}></use>
            </svg>
          </button>
          {children}
        </div>
      </div>
    </div>,
    targetElement
  );
};

export default Modal;
