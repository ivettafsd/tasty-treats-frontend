import React from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import {
  Button,
  TextField,
  FormGroup,
  FormControlLabel,
  TextareaAutosize,
  FormHelperText,
  FormControl,
} from "@mui/material";
import { makeStyles } from "@mui/styles";
import { Order } from "../../types/data";
import styles from "./OrderModal.module.scss";

const useStyles = makeStyles({
  root: {
    "& label .MuiFormControlLabel-root": {
      margin: 0,
    },
    "& .MuiFormControlLabel-label": {
      fontSize: "15px",
      fontWeight: "500",
      width: "100%",
      marginBottom: "8px",
      marginRight: "auto",
      borderRadius: "5px",
      color: "rgba(5, 5, 5, 0.5)",
    },
    "& .MuiTextField-root": {
      width: "100%",
    },
    "& textarea": {
      width: "100%",
      borderRadius: "5px",
      padding: "14px 18px",
      "& .MuiOutlinedInput-input": {
        padding: 0,
      },
    },
    "&.MuiFormControlLabel-root.MuiFormControlLabel-labelPlacementTop": {
      marginLeft: "0 !important",
      marginRight: "0 !important",
    },
  },
});

const OrderModal: React.FC = () => {
  const classes = useStyles();
  const f = useFormik({
    initialValues: {
      name: "",
      phone: "",
      email: "",
      comment: "",
    },
    validationSchema: Yup.object({
      name: Yup.string().required("Field is required"),
      phone: Yup.string()
        .required("Field is required")
        .matches(/(?=.*\+[0-9]{3}\s?[0-9]{2}\s?[0-9]{3}\s?[0-9]{4,5}$)/, {
          message: "Phone number is not valid",
        }),
      email: Yup.string()
        .required("Field is required")
        .matches(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/, { message: "Email is not valid" }),
      comment: Yup.string().required("Field is required"),
    }),
    onSubmit: async (values: Order) => {
      console.log("values", values);
    },
  });

  return (
    <>
      <h3 className={styles.title}>Order now</h3>
      <form onSubmit={f.handleSubmit}>
        <FormGroup>
          <FormControlLabel
            label="Name"
            labelPlacement="top"
            className={classes.root}
            control={
              <TextField
                error={f.errors?.name ? true : false}
                id="name"
                name="name"
                className={classes.root}
                value={f.values.name}
                onChange={f.handleChange}
                helperText={f.errors?.name ? f.errors?.name : " "}
              />
            }
          />
          <FormControlLabel
            label="Phone number"
            labelPlacement="top"
            className={classes.root}
            control={
              <TextField
                error={f.errors?.phone ? true : false}
                id="phone"
                name="phone"
                className={classes.root}
                value={f.values.phone}
                onChange={f.handleChange}
                helperText={f.errors?.phone ? f.errors?.phone : " "}
              />
            }
          />
          <FormControlLabel
            label="Email"
            labelPlacement="top"
            className={classes.root}
            control={
              <TextField
                error={f.errors?.email ? true : false}
                id="email"
                name="email"
                className={classes.root}
                value={f.values.email}
                onChange={f.handleChange}
                helperText={f.errors?.email ? f.errors?.email : " "}
              />
            }
          />
          <FormControl>
            <FormControlLabel
              label="Comment"
              labelPlacement="top"
              className={classes.root}
              control={
                <TextareaAutosize
                  minRows={3}
                  maxRows={6}
                  id="comment"
                  name="comment"
                  aria-label="comment"
                  className={classes.root}
                  value={f.values.comment}
                  onChange={f.handleChange}
                />
              }
            />
            <FormHelperText>{f.errors?.comment ? f.errors.comment : " "}</FormHelperText>
          </FormControl>
        </FormGroup>
        <div>
          <Button className={classes.root} color="primary" variant="contained" fullWidth type="submit">
            Send
          </Button>
        </div>
      </form>
    </>
  );
};

export default OrderModal;
