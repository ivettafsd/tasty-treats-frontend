import React, { Suspense, lazy, useEffect, useState } from "react";
import { Routes, Route } from "react-router-dom";
import Header from "./components/Header";
import Modal from "./components/Modal";
import Menu from "./components/Menu";
import OrderModal from "./components/OrderModal";


const HomePage = lazy(() => import("./pages/HomePage" /* webpackChunkName: "home-page" */));
const FavoritesPage = lazy(() => import("./pages/FavoritesPage" /* webpackChunkName: "favorites-page" */));

const App: React.FC = () => {
  const [isModalOpen, setIsModalOpen] = useState<Boolean>(false);
  const [isMenuOpen, setIsMenuOpen] = useState<Boolean>(false);

  useEffect(() => {
    const isExistLocalStorage = localStorage.getItem("favorites");
    if (!isExistLocalStorage) {
      localStorage.setItem("favorites", JSON.stringify([]));
    }
  }, []);

  const handleToggleOrderModal = () => {
    setIsModalOpen(prev => !prev);
  };

  const handleToggleMenu = () => {
   setIsMenuOpen((prev) => !prev);
  }

  return (
    <Suspense fallback={<p>njnjnj</p>}>
      <Header onToggleModal={handleToggleOrderModal} onToggleMenu={handleToggleMenu} />
      <Routes>
        <Route index element={<HomePage onToggleModal={handleToggleOrderModal} />} />
        <Route path="/favorites" element={<FavoritesPage />} />
      </Routes>
      {isModalOpen && (
        <Modal onClose={handleToggleOrderModal}>
          <OrderModal />
        </Modal>
      )}
      {isMenuOpen && (
        <Modal onClose={handleToggleMenu} fullVP={true}>
          <Menu onToggleMenu={handleToggleMenu} />
        </Modal>
      )}
    </Suspense>
  );
};

export default App;
