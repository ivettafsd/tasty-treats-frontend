import React, { Dispatch, SetStateAction } from "react";

export type Recipe = {
  _id: string;
  thumb: string;
  title: string;
  description: string;
  rating: number;
  category: string;
};
export type IngredientForDish = {
  id: string;
  measure: string;
  name: string;
  desc: string;
  img: string;
};
export type FullRecipe = {
  _id: string;
  title: string;
  category: string;
  area: string;
  instructions: string;
  description: string;
  thumb: string;
  preview: string;
  time: string;
  youtube: string;
  tags: string[];
  ingredients: IngredientForDish[];
  whoRated: number;
  rating: number;
};

export type Event = {
  _id: string;
  cook: {
    name: string;
    imgUrl: string;
    imgWebpUrl: string;
  };
  topic: {
    name: string;
    area: string;
    previewUrl: string;
    previewWebpUrl: string;
    imgUrl: string;
    imgWebpUrl: string;
  };
};

export type PopularRecipe = {
  _id: string;
  preview: string;
  thumb: string;
  title: string;
  description: string;
  popularity: number;
};

export type Category = {
  _id: string;
  name: string;
};

export type Ingredient = {
  _id: string;
  desc: string;
  img: string;
  name: string;
};

export type Area = {
  _id: string;
  name: string;
};
export type Order = {
  name: string;
  phone: string;
  email: string;
  comment: string;
};

export type HeaderProps = {
  onToggleModal: () => void;
  onToggleMenu: () => void;
};
export type MenuProps = {
  onToggleMenu: () => void;
};
export type NavigationProps = {
  onToggleMenu: () => void;
};
export type HomePageProps = {
  onToggleModal: () => void;
};
export type HeroProps = {
  onToggleModal: () => void;
};
export interface RecipesListProps {
  recipes: Recipe[];
}
export interface RecipeItemProps {
  recipe: Recipe;
}

export interface RecipeModalProps {
  recipe: FullRecipe | null;
}

export interface BasicPaginationProps {
  totalPages: number;
  setCurrentPage: React.Dispatch<React.SetStateAction<number>>;
}

export interface ToolbarProps {
  setCurrentSearch: Dispatch<SetStateAction<string>>;
  currentArea: string;
  setCurrentArea: Dispatch<SetStateAction<string>>;
  currentIngredient: string;
  setCurrentIngredient: Dispatch<SetStateAction<string>>;
  currentTime: string;
  setCurrentTime: Dispatch<SetStateAction<string>>;
}

export interface ModalProps {
  onClose: () => void;
  children: React.ReactNode;
  fullVP?: Boolean
}
