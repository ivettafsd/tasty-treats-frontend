import axios from "axios";
const BASE_URL = process.env.REACT_APP_BASE_URL_API;
axios.defaults.baseURL = BASE_URL;

export const getRecipes = async ({
  title = "",
  category = "",
  area = "",
  ingredient = "",
  time = "",
  page = 1,
  limit = 9,
}) => {
  try {
    const { data } = await axios.get(
      `/recipes?title=${title}&category=${category}&area=${area}&ingredient=${ingredient}&time=${time}&page=${page}&limit=${limit}`
    );

    // showSuccessMessage('Вы успешно зарегистрировались!');
    return data;
  } catch (error) {
    // showErrorMessage(error.message);
    return null;
  }
};

export const getPopularRecipes = async () => {
  try {
    const { data } = await axios.get(`/recipes/popular`);
    return data;
  } catch (error) {
    // showErrorMessage(error.message);
    return null;
  }
};
export const getRecipeById = async (id) => {
  try {
    const { data } = await axios.get(`/recipes/${id}`);
    return data;
  } catch (error) {
    // showErrorMessage(error.message);
    return null;
  }
};
export const getCategories = async () => {
  try {
    const { data } = await axios.get(`/categories`);
    return data;
  } catch (error) {
    // showErrorMessage(error.message);
    return null;
  }
};
export const getAreas = async () => {
  try {
    const { data } = await axios.get(`/areas`);
    return data;
  } catch (error) {
    // showErrorMessage(error.message);
    return null;
  }
};
export const getIngredients = async () => {
  try {
    const { data } = await axios.get(`/ingredients`);
    return data;
  } catch (error) {
    // showErrorMessage(error.message);
    return null;
  }
};
export const getEvents = async () => {
  try {
    const { data } = await axios.get(`/events`);
    return data;
  } catch (error) {
    // showErrorMessage(error.message);
    return null;
  }
};
