import React, { useState, useEffect } from "react";
import RecipeItem from "../components/RecipeItem";
import { Recipe } from "../types/data";
import styles from "./FavoritesPage.module.scss";

const FavoritesPage: React.FC = () => {
  const [favorites, setFavorites] = useState<Recipe[]>([]);

  useEffect(() => {
    const favoritesData: string | null = localStorage.getItem("favorites");
    const currentFavorites = favoritesData ? JSON.parse(favoritesData) : [];
    setFavorites(currentFavorites);
  }, []);

  return (
    <section>
      <ul>
        {favorites.length > 0 &&
          favorites.reduce((acc: string[], favorite: Recipe) => {
            if (!acc.includes(favorite.category)) {
              acc.push(favorite.category);
            }
            return acc;
          }, [])}
      </ul>
      <ul className={styles.list}>
        {favorites.length > 0 &&
          favorites.map((favorite: Recipe) => (
            //   <li key={favorite._id}>
            //     <img src={favorite.preview} />
            //     <button
            //       type="button"
            //       //onClick={handleToggleFavorites}
            //     >
            //       Heart
            //     </button>
            //     <h3>{favorite.title}</h3>
            //     <p>{favorite.description.split(0, 100) + "..."}</p>
            //     <input type="range" max="5" defaultValue={favorite.rating} onChange={() => {}} />
            //     <button
            //       type="button"
            //       //onClick={handleOpenRecipeModal}
            //     >
            //       See recipe
            //     </button>
            //   </li>
            // )
       
              <RecipeItem recipe={favorite} key={favorite._id} />
      
          ))}
      </ul>
    </section>
  );
};

export default FavoritesPage;
