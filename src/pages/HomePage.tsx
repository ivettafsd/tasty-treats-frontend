import React, { useState } from "react";
import { useAsync } from "react-use";
import Hero from "../components/Hero";
import Categories from "../components/Categories";
import Toolbar from "../components/Toolbar";
import RecipeList from "../components/RecipesList";
import BasicPagination from "../components/BasicPagination";
import PopularRecipes from "../components/PopularRecipes";
import { getRecipes } from "../services";
import { Recipe, HomePageProps } from "../types/data";
import styles from './HomePage.module.scss';

const HomePage: React.FC<HomePageProps> = ({ onToggleModal }) => {
  const [currentCategory, setCurrentCategory] = useState<string | null>(null);
  const [currentSearch, setCurrentSearch] = useState<string>("");
  const [currentArea, setCurrentArea] = useState<string>("");
  const [currentIngredient, setCurrentIngredient] = useState<string>("");
  const [currentTime, setCurrentTime] = useState<string>("");
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [recipes, setRecipes] = useState<Recipe[]>([]);
  const [totalPages, setTotalPages] = useState<number>(10);

  useAsync(async () => {
    const filtersData: {
      category?: string;
      title?: string;
      area?: string;
      ingredient?: string;
      time?: string;
      page?: number;
    } = {};

    if (currentCategory) filtersData.category = currentCategory;
    if (currentSearch) filtersData.title = currentSearch;
    if (currentArea) filtersData.area = currentArea;
    if (currentIngredient) filtersData.ingredient = currentIngredient;
    if (currentTime) filtersData.time = currentTime;
    if (currentPage) filtersData.page = currentPage;

    const dataRecipes = await getRecipes(filtersData);

    setRecipes(dataRecipes.results);
    setTotalPages(dataRecipes.totalPages);
  }, [currentCategory, currentSearch, currentArea, currentIngredient, currentTime, currentPage]);


  return (
    <>
      <Hero onToggleModal={onToggleModal} />
      <section className={styles.content}>
        <Categories
          onClick={setCurrentCategory}
          currentCategory={currentCategory}
        />
        <PopularRecipes />
        <Toolbar
          setCurrentSearch={setCurrentSearch}
          currentArea={currentArea}
          setCurrentArea={setCurrentArea}
          currentIngredient={currentIngredient}
          setCurrentIngredient={setCurrentIngredient}
          currentTime={currentTime}
          setCurrentTime={setCurrentTime}
        />
        <RecipeList recipes={recipes} />
        <BasicPagination totalPages={totalPages} setCurrentPage={setCurrentPage} />
      </section>
    </>
  );
};

export default HomePage;
